/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Modelo.EnteroGrande;

/**
 *
 * @author Lab05pcdocente
 */
public class TestVector {

    public static void main(String[] args) {
        Consola2 consola = new Consola2();
        //EnteroGrande vector=new EnteroGrande(new Consola2().leerEntero("Digite tamaño del vector"));

        try {
            int n = consola.leerEntero("Digite tamaño del vector");
            EnteroGrande vector = new EnteroGrande(n);
            System.out.println(vector.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
